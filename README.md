# Overview

This is an example Java Web application, using Java Servlet and JSP technologies. The app performs basic CRUD operations against a database.

# Features

1. User authentication using Web Container Realm (Tomcat + SQLite).
2. CRUD operations against a MySQL database, using Spring Jdbc Template.
3. MVC Model 2, without the Front Controller pattern - controller layer implemented using multiple servlets.

# Screenshots

## Login

![Login](readme/warehouse_login.JPG)

## Product List

![Product List](readme/warehouse_product_list.JPG)

## Product Form

![Product Form](readme/warehouse_product_form.JPG)