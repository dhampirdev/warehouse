package dh.warehouse.app;

import dh.warehouse.product.ProductFactory;
import dh.warehouse.product.dao.MysqlRepository;
import dh.warehouse.product.dao.Repository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.validation.Validation;
import javax.validation.Validator;

@Configuration
@ComponentScan(basePackages = {"dh.warehouse"})
public class AppConfig {

    @Bean
    public DataSource mysqlDataSource() throws NamingException {
        JndiTemplate jndiTemplate = new JndiTemplate();
        return (DataSource) jndiTemplate.lookup("java:comp/env/jdbc/warehouse");
    }

    @Bean
    public JdbcTemplate mysqlTemplate(DataSource mysqlDataSource) {
        return new JdbcTemplate(mysqlDataSource);
    }

    @Bean
    public Validator validator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
