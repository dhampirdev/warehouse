package dh.warehouse.app.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import dh.warehouse.view.Util;

@WebServlet(
        name = "dashboard",
        urlPatterns = {"/dashboard"}
)
@ServletSecurity(
        @HttpConstraint(
                rolesAllowed = {"Admin"}
        )
)
public class Dashboard extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher(Util.getViewPath("dashboard"));
        dispatcher.forward(req, resp);
    }
}
