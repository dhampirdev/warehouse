package dh.warehouse.app.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name="logout",
        urlPatterns = {"/logout"}
)
@ServletSecurity(
        @HttpConstraint(
                rolesAllowed = {"Admin"}
        )
)
public class Logout extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.logout();
        req.getSession().invalidate();

        //TODO: Log events

        resp.sendRedirect("dashboard");
    }
}
