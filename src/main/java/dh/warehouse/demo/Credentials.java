package dh.warehouse.demo;

public class Credentials {

    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin123";

    /**
     * @return demo user name.
     */
    public String getUsername() {
        return USERNAME;
    }

    /**
     * @return demo user password.
     */
    public String getPassword() {
        return PASSWORD;
    }

}
