package dh.warehouse.http;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class FlashAttributes {

    private String SESSION_KEY_PREFIX = "flash.";

    private HttpSession session;

    public FlashAttributes(HttpSession session) {
        this.session = session;
    }

    /**
     * Constructs flash attribute name out of regular name by adding a constant prefix.
     *
     * @param name regular attribute name.
     * @return flash attribute name.
     */
    private String toFlashName(String name) {
        return SESSION_KEY_PREFIX + name;
    }

    /**
     * @param name attribute name.
     * @param value attribute value.
     */
    public void addAttribute(String name, Object value) {
        session.setAttribute(toFlashName(name), value);
    }

    /**
     * Fetches, deletes and finally return an attribute.
     *
     * @param name attribute name.
     * @return attribute value if found and null otherwise.
     */
    public Object popAttribute(String name) {
        Object value = session.getAttribute(toFlashName(name));
        session.removeAttribute(toFlashName(name));

        return value;
    }

    /**
     * @return list of all flash attributes existing in session.
     */
    public ArrayList<String> getAttributeNames() {
        ArrayList<String> flashAttributeNames = new ArrayList<>();

        session
            .getAttributeNames()
            .asIterator()
            .forEachRemaining(
                    (String name) -> {
                        if (name.startsWith(SESSION_KEY_PREFIX))
                            flashAttributeNames.add(name.replace(SESSION_KEY_PREFIX, ""));
                    }
            );

        return flashAttributeNames;
    }
}
