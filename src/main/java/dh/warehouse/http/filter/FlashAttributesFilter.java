package dh.warehouse.http.filter;

import dh.warehouse.http.FlashAttributes;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebFilter(
        filterName = "FlashAttributesFilter",
        urlPatterns = {"/*"}
)
public class FlashAttributesFilter implements Filter{

    private FlashAttributes flashAttributes;

    private HttpSession getHttpSession(ServletRequest request) {
        return ((HttpServletRequest)request).getSession();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = getHttpSession(servletRequest);
        flashAttributes = new FlashAttributes(session);

        ArrayList<String> foundAttributes = flashAttributes.getAttributeNames();
        foundAttributes.forEach((String name) -> servletRequest.setAttribute(name, flashAttributes.popAttribute(name)));

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
