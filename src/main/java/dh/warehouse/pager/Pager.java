package dh.warehouse.pager;

public class Pager {

    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 10;

    private final static int DEFAULT_ITEM_COUNT = 0;

    private int pageNumber;
    private int pageSize;

    private int totalItemCount;

    public Pager() {
        pageNumber = DEFAULT_PAGE_NUMBER;
        pageSize = DEFAULT_PAGE_SIZE;
        totalItemCount = DEFAULT_ITEM_COUNT;
    }

    /**
     * @param pageNumber new page number, must be greater than zero.
     */
    public void setPageNumber(int pageNumber) {
        if (pageNumber >= 1) {
            this.pageNumber = pageNumber;
        }
    }

    /**
     * @param pageSize new page size, must be greater than zero.
     */
    public void setPageSize(int pageSize) {
        if (pageSize >= 1) {
            this.pageSize = pageSize;
        }
    }

    /**
     * @param totalCount new total number of pageable items, must be greater than zero.
     */
    public void setTotalItemCount(int totalCount) {
        if (totalCount >= 1) {
            this.totalItemCount = totalCount;
        }
    }

    /**
     * @return this page number.
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * @return this page size.
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @return calculated item list offset.
     */
    public int getPageOffset() {
        return (pageNumber - 1) * pageSize;
    }

    /**
     * @return true if previous page number points to a valid page, false otherwise.
     */
    public boolean hasPreviousPage() {
        return (pageNumber - 1) > 0 && totalItemCount - (pageNumber - 1) * pageSize > 0;
    }

    /**
     * @return true if next page number points to a valid page, false otherwise.
     */
    public boolean hasNextPage() {
        return totalItemCount - pageNumber * pageSize > 0;
    }

    /**
     * @return number of the previous page, or this page if previous is not accessible.
     */
    public int getPreviousPageNumber() {
        if (hasPreviousPage()) {
            return pageNumber - 1;
        } else {
            return pageNumber;
        }
    }

    /**
     * @return number of the next page, or this page if next is not accessible.
     */
    public int getNextPageNumber() {
        if (hasNextPage()) {
            return pageNumber + 1;
        } else {
            return pageNumber;
        }
    }
}
