package dh.warehouse.pager.filter;

import dh.warehouse.pager.Pager;
import dh.warehouse.product.dao.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Optional;

@WebFilter(
        filterName = "PagerFilter",
        urlPatterns = {"/product/list"}
)
public class PagerFilter implements Filter {

    private final static String PAGE_NUMBER_PARAM_CODE = "p";
    private final static String PAGE_SIZE_PARAM_CODE = "c";

    @Autowired
    private Repository productRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, filterConfig.getServletContext());
    }

    /**
     * @param servletRequest current request.
     * @param paramName      target parameter value.
     * @return value parsed from the request, -1 if parsing failed or value was not present.
     */
    private int getIntegerParamValue(ServletRequest servletRequest, String paramName) {
        String paramValue = Optional
                .ofNullable(servletRequest.getParameter(paramName))
                .orElse("");

        if (paramValue.length() < 1) {
            return -1;
        }

        int parsedValue;
        try {
            parsedValue = Integer.parseInt(paramValue);
        } catch (NumberFormatException e) {
            parsedValue = -1;
        }

        return parsedValue;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        int pageNumber = getIntegerParamValue(servletRequest, PAGE_NUMBER_PARAM_CODE);
        int pageSize = getIntegerParamValue(servletRequest, PAGE_SIZE_PARAM_CODE);

        Pager pager = new Pager();
        pager.setPageNumber(pageNumber);
        pager.setPageSize(pageSize);
        pager.setTotalItemCount(productRepository.getTotalCount());

        servletRequest.setAttribute("pager", pager);

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
