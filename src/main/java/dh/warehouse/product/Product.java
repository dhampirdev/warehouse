package dh.warehouse.product;

import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.PositiveOrZero;
import java.util.Optional;

/**
 * POJO representation of a product.
 */
public class Product{

    private ID id = null;

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9_\\-]+$")
    private String sku;

    @NotEmpty
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String name;

    @PositiveOrZero
    private double price;

    @PositiveOrZero
    private int qty;

    /**
     * @return optional ID if set or default ID otherwise.
     */
    private ID getOptionalId() {
        return Optional.ofNullable(id).orElse(new ID());
    }

    /**
     * @return true if this represent a new record, false if existing.
     */
    public boolean isNew(){
        return getOptionalId().undefined;
    }

    /**
     * @param id new ID.
     */
    public void setId(long id) {
        this.id = new ID(id);
    }

    /**
     * @return this ID.
     */
    public long getId() {
        return getOptionalId().id;
    }

    /**
     * @param sku new SKU.
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * @return this SKU.
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param name new name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return this name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param price new price.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return this price.
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param qty new quantity.
     */
    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * @return this quantity.
     */
    public int getQty() {
        return qty;
    }

    /**
     * Wrapper for optional product ID.
     *
     * If actual ID is present then undefined flag will not be set.
     * If no real ID is set then undefined flag will be raised.
     */
    private class ID {
        long id;
        boolean undefined;

        ID() {
            id = 0;
            undefined = true;
        }

        ID(long id) {
            this.id = id;
            undefined = false;
        }
    }
}