package dh.warehouse.product;

import org.springframework.stereotype.Component;

@Component("productFactory")
public class ProductFactory {
    /**
     * Creates new "empty" instance.
     *
     * @return new "empty" instance.
     */
    public Product create() {
        Product blank = new Product();

        blank.setSku("");
        blank.setName("");
        blank.setPrice(0.0);
        blank.setQty(0);

        return blank;
    }

    /**
     * Creates new instance without an ID.
     *
     * @param sku new SKU.
     * @param name new name.
     * @param price new price.
     * @param qty new quantity.
     * @return new filled instance.
     */
    public Product create(String sku, String name, double price, int qty) {
        Product product = new Product();

        product.setSku(sku);
        product.setName(name);
        product.setPrice(price);
        product.setQty(qty);

        return product;
    }

    /**
     * Creates new instance with all fields set.
     *
     * @param id new ID.
     * @param sku new SKU.
     * @param name new name.
     * @param price new price.
     * @param qty new quantity.
     * @return new filled instance.
     */
    public Product create(long id, String sku, String name, double price, int qty) {
        Product product = new Product();

        product.setId(id);
        product.setSku(sku);
        product.setName(name);
        product.setPrice(price);
        product.setQty(qty);

        return product;
    }
}
