package dh.warehouse.product.dao;

import dh.warehouse.product.Product;
import dh.warehouse.product.ProductFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component("productRepository")
public class MysqlRepository implements Repository {

    private final static String SQL_SELECT_ALL
            = "SELECT * FROM product";
    private final static String SQL_SELECT_PAGE
            = "SELECT * FROM product LIMIT ?, ?";
    private final static String SQL_SELECT_BY_ID
            = "SELECT * FROM product WHERE id = ?";
    private final static String SQL_DELETE_BY_ID
            = "DELETE FROM product WHERE id = ?";
    private final static String SQL_INSERT
            = "INSERT INTO product (sku, name, price, qty) VALUES (?, ?, ?, ?)";
    private final static String SQL_UPDATE_BY_ID
            = "UPDATE product SET sku = ?, name = ?, price = ?, qty = ? WHERE id = ?";
    private final static String SQL_COUNT
            = "SELECT COUNT(id) FROM product";

    private JdbcOperations jdbcOperations;
    private ProductFactory productFactory;

    @Autowired
    public MysqlRepository(JdbcOperations jdbcOperations, ProductFactory productFactory) {
        this.jdbcOperations = jdbcOperations;
        this.productFactory = productFactory;
    }

    @Override
    public Map<Long, Product> getAll() {
        List<Product> productList = jdbcOperations.query(SQL_SELECT_ALL, new ProductMapper());

        // TODO: investigate sorting problem

        return productList.stream().collect(Collectors.toMap(Product::getId, product -> product));
    }

    @Override
    public Map<Long, Product> getAll(int offset, int limit) {
        List<Product> productList = jdbcOperations.query(
                SQL_SELECT_PAGE,
                new ProductMapper(),
                offset,
                limit
        );

        return productList.stream().collect(Collectors.toMap(Product::getId, product -> product));
    }

    @Override
    public Optional<Product> get(long id) {
        Optional<Product> result;
        try {
            Product product = jdbcOperations.queryForObject(SQL_SELECT_BY_ID, new Long[]{id}, new ProductMapper());
            result = Optional.of(product);

        } catch (EmptyResultDataAccessException e) {
            result = Optional.empty();
        }

        return result;
    }

    @Override
    public boolean delete(long productId) {
        return jdbcOperations.update(SQL_DELETE_BY_ID, productId) == 1;
    }

    @Override
    public boolean save(Product product) {
        if (product.isNew()) {
            return create(product);
        } else {
            return update(product);
        }
    }

    /**
     * Inserts new record into this repository.
     *
     * @param product representation of new record.
     * @return true if changes were made, false otherwise.
     */
    private boolean create(Product product) {
        return jdbcOperations.update(
                SQL_INSERT,
                product.getSku(),
                product.getName(),
                product.getPrice(),
                product.getQty()
        ) == 1;
    }

    /**
     * Updates existing record in this repository.
     *
     * @param product representation of existing record.
     * @return true if changes were made, false otherwise.
     */
    private boolean update(Product product) {
        return jdbcOperations.update(
                SQL_UPDATE_BY_ID,
                product.getSku(),
                product.getName(),
                product.getPrice(),
                product.getQty(),
                product.getId()
        ) == 1;
    }

    private class ProductMapper implements RowMapper<Product> {
        @Override
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
            return productFactory.create(
                    rs.getLong("id"),
                    rs.getString("sku"),
                    rs.getString("name"),
                    rs.getDouble("price"),
                    rs.getInt("qty")
            );
        }
    }

    @Override
    public int getTotalCount() {
        return jdbcOperations.queryForObject(SQL_COUNT, Integer.class);
    }
}
