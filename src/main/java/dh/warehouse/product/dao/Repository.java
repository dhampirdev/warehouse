package dh.warehouse.product.dao;

import dh.warehouse.product.Product;

import java.util.Map;
import java.util.Optional;

public interface Repository {
    /**
     * Fetches all existing products.
     *
     * @return product map (key = product ID, value = product record).
     */
    Map<Long, Product> getAll();

    /**
     * Fetches limited number of products.
     *
     * @param offset starting point for the limited list.
     * @param limit  number of items to fetch.
     * @return products matching this page.
     */
    Map<Long, Product> getAll(int offset, int limit);

    /**
     * Fetches single product by ID.
     *
     * If ID exists then matching record is returned.
     * If ID is not found Optional#empty is returned.
     *
     * @param id requested record ID.
     * @return matching Product instance or Optional#empty.
     */
    Optional<Product> get(long id);

    /**
     * Deletes single product by ID.
     *
     * @param id requested record ID.
     * @return true if product was deleted, false otherwise.
     */
    boolean delete(long id);

    /**
     * Persists product changes in this repository.
     *
     * @param product existing product to update or new product to create.
     * @return true if operation was successful, false otherwise.
     */
    boolean save(Product product);

    /**
     * Calculates the total number of products that exist in this repository.
     *
     * @return total count of existing products.
     */
    int getTotalCount();
}
