package dh.warehouse.product.servlet;

import dh.warehouse.http.FlashAttributes;
import dh.warehouse.product.ProductFactory;
import dh.warehouse.product.dao.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public abstract class Abstract extends HttpServlet {

    @Autowired
    Repository productRepository;

    @Autowired
    ProductFactory productFactory;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    FlashAttributes flashAttributes(HttpServletRequest request) {
        return new FlashAttributes(request.getSession());
    }
}
