package dh.warehouse.product.servlet;

import dh.warehouse.product.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(
        name = "ProductDelete",
        urlPatterns = {"/product/delete"}
)
@ServletSecurity(
        @HttpConstraint(
                rolesAllowed = {"Admin"}
        )
)
public class Delete extends Abstract {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long productId;
        try {
            productId = Long.parseLong(req.getParameter("id"));
        } catch (NumberFormatException e) {
            productId = -1L;
        }

        Optional<Product> product = productRepository.get(productId);

        if (product.isPresent() && productRepository.delete(productId)) {
            flashAttributes(req).addAttribute("successMessage", "Product has been deleted.");
        } else {
            flashAttributes(req).addAttribute("errorMessage", "Could not delete requested product!");
        }

        resp.sendRedirect("/product/list");
    }
}
