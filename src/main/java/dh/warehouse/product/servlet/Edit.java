package dh.warehouse.product.servlet;

import dh.warehouse.product.Product;
import dh.warehouse.product.servlet.form.FormData;
import dh.warehouse.view.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(
        name = "ProductEdit",
        urlPatterns = {"/product/edit"}
)
@ServletSecurity(
        @HttpConstraint(
                rolesAllowed = {"Admin"}
        )
)
public class Edit extends Abstract {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long productId;
        try {
            productId = Long.parseLong(req.getParameter("id"));
        } catch (NumberFormatException e) {
            productId = -1L;
        }

        Product product = productRepository
                .get(productId)
                .orElse(productFactory.create());

        FormData formData = Optional
                .ofNullable((FormData) req.getAttribute("formData"))
                .orElse(new FormData(product));

        if (!product.isNew()) {
            req.setAttribute("pageTitle", "Edit Product #" + productId);
            req.setAttribute("productId", productId);
            req.setAttribute("formData", formData);

            req.getRequestDispatcher(Util.getViewPath("product/edit")).forward(req, resp);
        } else {
            flashAttributes(req).addAttribute("errorMessage", "Could not find requested product!");
            resp.sendRedirect("/product/list");
        }
    }
}
