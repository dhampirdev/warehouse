package dh.warehouse.product.servlet;

import dh.warehouse.pager.Pager;
import dh.warehouse.product.Product;
import dh.warehouse.view.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@WebServlet(
        name = "ProductList",
        urlPatterns = {"/product/list"}
)
@ServletSecurity(
        @HttpConstraint(
                rolesAllowed = {"Admin"}
        )
)
public class List extends Abstract {

    /**
     * @param req current request.
     * @return existing or default pager.
     */
    private Pager getPager(HttpServletRequest req) {
        return Optional
                .ofNullable((Pager) req.getAttribute("pager"))
                .orElse(new Pager());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Pager pager = getPager(req);

        Map<Long, Product> products = productRepository.getAll(pager.getPageOffset(), pager.getPageSize());
        req.setAttribute("products", products);

        RequestDispatcher dispatcher = req.getRequestDispatcher(Util.getViewPath("product/list"));
        dispatcher.forward(req, resp);
    }
}
