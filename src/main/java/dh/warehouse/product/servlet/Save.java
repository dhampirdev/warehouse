package dh.warehouse.product.servlet;

import dh.warehouse.product.Product;
import dh.warehouse.product.servlet.form.FormData;
import dh.warehouse.product.servlet.request.SaveValidator;
import dh.warehouse.product.validation.ProductValidator;
import dh.warehouse.product.validation.ProductValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "ProductSave",
        urlPatterns = {"/product/save"}
)
@ServletSecurity(
        @HttpConstraint(
                rolesAllowed = {"Admin"}
        )
)
public class Save extends Abstract {

    @Autowired
    private SaveValidator reqValidator;

    @Autowired
    private ProductValidatorFactory productValidatorFactory;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (!reqValidator.hasRequiredFields(req)
                || (reqValidator.hasId(req)) && !reqValidator.hasValidId(req)) {
            flashAttributes(req).addAttribute("errorMessage", "Invalid request.");
            resp.sendRedirect("/product/list");
            return;
        }

        FormData formData = new FormData(req);
        flashAttributes(req).addAttribute("formData", formData);

        if (!reqValidator.isPriceFormatValid(req) || !reqValidator.isQtyFormatValid(req)) {
            flashAttributes(req).addAttribute("errorMessage", "Invalid request. Check data format.");
            if (reqValidator.hasId(req)) {
                resp.sendRedirect("/product/edit?id=" + req.getParameter("id"));
            } else {
                resp.sendRedirect("/product/add");
            }
            return;
        }

        Product candidate = productRepository
                .get(formData.getIdAsLong())
                .orElse(productFactory.create());

        candidate.setSku(formData.getSku());
        candidate.setName(formData.getName());
        candidate.setPrice(formData.getPriceAsDouble());
        candidate.setQty(formData.getQtyAsInteger());

        ProductValidator productValidator = productValidatorFactory.create(candidate);
        productValidator.validate();

        boolean saved = false;
        if (productValidator.isValid()) {
            try {
                if (saved = productRepository.save(candidate)) {
                    flashAttributes(req).addAttribute("successMessage", "Product has been saved.");
                } else {
                    flashAttributes(req).addAttribute("errorMessage", "Could not saved the product.");
                }
            } catch (DuplicateKeyException e) {
                flashAttributes(req).addAttribute("errorMessage", "The product is a duplicate and cannot be saved.");
            }
        } else {
            flashAttributes(req).addAttribute("formErrors", productValidator.getErrors());
            flashAttributes(req).addAttribute("errorMessage", "Invalid product data.");
        }

        if (saved) {
            resp.sendRedirect("/product/list");
        } else if (reqValidator.hasId(req)) {
            resp.sendRedirect("/product/edit?id=" + req.getParameter("id"));
        } else {
            resp.sendRedirect("/product/add");
        }
    }
}
