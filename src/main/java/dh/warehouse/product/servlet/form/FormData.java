package dh.warehouse.product.servlet.form;

import dh.warehouse.product.Product;

import javax.servlet.http.HttpServletRequest;

public class FormData {

    private String id;
    private String sku;
    private String name;
    private String price;
    private String qty;

    public FormData(HttpServletRequest req) {
        id = req.getParameter("id");
        sku = req.getParameter("sku");
        name = req.getParameter("name");
        price = req.getParameter("price");
        qty = req.getParameter("qty");
    }

    public FormData(Product product) {
        id = String.valueOf(product.getId());
        sku = product.getSku();
        name = product.getName();
        price = String.valueOf(product.getPrice());
        qty = String.valueOf(product.getQty());
    }

    /**
     * @return this ID.
     */
    public String getId() {
        return id;
    }

    /**
     * @return this SKU.
     */
    public String getSku() {
        return sku;
    }

    /**
     * @return this name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return this price.
     */
    public String getPrice() {
        return price;
    }

    /**
     * @return this qty.
     */
    public String getQty() {
        return qty;
    }

    /**
     * @return this ID parsed to long, or 0 if unable to parse.
     */
    public long getIdAsLong() {
        long id;
        try {
            id = Long.parseLong(this.id);
        } catch (NumberFormatException e) {
            id = 0L;
        }

        return id;
    }

    /**
     * @return this price parsed to double, or 0.0 is unable to parse.
     */
    public double getPriceAsDouble() {
        double price;
        try {
            price = Double.parseDouble(this.price);
        } catch (NumberFormatException e) {
            price = 0.0;
        }

        return price;
    }

    /**
     * @return this qty parsed to integer, or 0 if unable to parse.
     */
    public int getQtyAsInteger() {
        int qty;
        try {
            qty = Integer.parseInt(this.qty);
        } catch (NumberFormatException e) {
            qty = 0;
        }

        return qty;
    }
}
