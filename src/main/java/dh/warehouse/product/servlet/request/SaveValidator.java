package dh.warehouse.product.servlet.request;

import dh.warehouse.product.dao.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Component("saveRequestValidator")
public class SaveValidator {

    @Autowired
    Repository productRepository;

    /**
     * @param req HTTP request for save action.
     * @return true if all required fields are present, false otherwise.
     */
    public boolean hasRequiredFields(HttpServletRequest req) {
        Supplier<Stream<Field>> requiredFieldsSupplier
                = () -> Arrays.stream(Field.values()).filter((Field field) -> field != Field.ID);

        long numRequired = requiredFieldsSupplier.get().count();
        long numPresent = requiredFieldsSupplier.get()
                .filter(
                        (Field field) -> req.getParameter(field.toString()) != null
                                && req.getParameter(field.toString()).length() > 0
                ).count();

        return numPresent == numRequired;
    }

    /**
     * @param req HTTP request for save action.
     * @return true if ID field is present, false otherwise.
     */
    public boolean hasId(HttpServletRequest req) {
        return Optional
                .ofNullable(req.getParameter(Field.ID.toString()))
                .orElse("")
                .length() > 0;
    }

    /**
     * @param req HTTP request for save action.
     * @return true if ID field exists and points to a valid product, false otherwise.
     */
    public boolean hasValidId(HttpServletRequest req) {
        long id = Long.parseLong(req.getParameter(Field.ID.toString()));

        return productRepository.get(id).isPresent();
    }

    /**
     * @param req HTTP request for save action.
     * @return true if price field can be parsed to a double, false otherwise.
     */
    public boolean isPriceFormatValid(HttpServletRequest req) {
        try {
            Double.parseDouble(req.getParameter(Field.PRICE.toString()));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * @param req HTTP request for save action.
     * @return true if qty field can be parsed to an integer, false otherwise.
     */
    public boolean isQtyFormatValid(HttpServletRequest req) {
        try {
            Integer.parseInt(req.getParameter(Field.QTY.toString()));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Enumerates expected request fields.
     */
    private enum Field {

        ID("id"), SKU("sku"), NAME("name"), PRICE("price"), QTY("qty");

        private String fieldName;

        Field(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public String toString() {
            return fieldName;
        }
    }
}
