package dh.warehouse.product.servlet.tag;

import dh.warehouse.product.validation.Error;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Arrays;

public class FormErrorTag extends TagSupport {

    private String field;

    /**
     * @param field this field to lookup.
     */
    public void setField(String field) {
        this.field = field;
    }

    @Override
    public int doStartTag() throws JspException {
        Object errors = pageContext.getRequest().getAttribute("formErrors");
        if (errors != null && errors.getClass().isArray() && errors instanceof Error[]) {
            Error fieldError = Arrays
                    .stream((Error[]) errors)
                    .filter((Error error) -> error.getField().contentEquals(field))
                    .findFirst()
                    .orElse(new Error("", ""));

            try {
                pageContext.getOut().print(fieldError.getMessage());
            } catch (IOException e) {
                throw new JspException("Error: " + e.getMessage());
            }
        }

        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
