package dh.warehouse.product.validation;

public class Error {

    private String field;
    private String message;

    public Error(String field, String message) {
        this.field = field;
        this.message = message;
    }

    /**
     * @return this target field name.
     */
    public String getField() {
        return field;
    }

    /**
     * @return this message.
     */
    public String getMessage() {
        return message;
    }
}
