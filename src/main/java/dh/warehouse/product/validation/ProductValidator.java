package dh.warehouse.product.validation;

import dh.warehouse.product.Product;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Set;

public class ProductValidator {

    private Validator validator;

    private Product target;
    private ArrayList<Error> errors;

    ProductValidator(Product target, Validator validator) {
        this.validator = validator;

        this.target = target;
        this.errors = new ArrayList<>();
    }

    /**
     * Fetches all (if any) errors from last validation call.
     *
     * @return list of most recent validation errors (if any).
     */
    public Error[] getErrors() {
        Error[] errors = new Error[this.errors.size()];
        this.errors.toArray(errors);

        return errors;
    }

    /**
     * Validates this target and stores constraints violations (if any) as this errors.
     */
    public void validate() {
        saveErrors(validator.validate(target));
    }

    /**
     * @return true if there were no errors, false otherwise.
     */
    public boolean isValid() {
        return errors.isEmpty();
    }

    /**
     * Converts constraint violations to errors.
     *
     * @param violations list of constraint violations from last validation.
     */
    private void saveErrors(Set<ConstraintViolation<Product>> violations) {
        errors.clear();

        for (ConstraintViolation<Product> v : violations) {
            errors.add(new Error(v.getPropertyPath().toString(), v.getMessage()));
        }
    }
}
