package dh.warehouse.product.validation;

import dh.warehouse.product.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.validation.Validator;

@Component("productValidatorFactory")
public class ProductValidatorFactory {

    @Autowired
    private Validator validator;

    /**
     * Creates validator for target product.
     *
     * @param target product to validate.
     * @return validator for this product.
     */
    public ProductValidator create(Product target) {
        return new ProductValidator(target, validator);
    }
}
