package dh.warehouse.view;

import java.util.regex.*;

public class Util {

    private static final String VIEWS_BASE_DIR = "/WEB-INF/views/";
    private static final String VIEWS_FILE_EXT = ".jsp";

    /**
     * Checks if path is valid, without leading or trailing slashes, trailing extension or unsupported characters.
     *
     * Examples of valid paths:
     * - foo-BAR-1
     * - foo/BAR-bar-2
     *
     * Examples of invalid paths:
     * - /foo/bar (leading slash)
     * - foo/bar/ (trailing slash)
     * - foo.jsp (includes file extension)
     *
     * @param path - view path relative to views base.
     * @return validation result.
     */
    private static boolean isViewPathValid(String path) {
        Pattern pattern = Pattern.compile("^([\\w\\-]+(/[\\w\\-]+)*)$");
        Matcher matcher = pattern.matcher(path);

        return matcher.matches();
    }

    /**
     * Translates view path relative to views base directory, to path relative to web context root.
     *
     * @param view - view path relative to views base dir.
     * @return view path relative to web context root.
     */
    public static String getViewPath(String view) throws IllegalArgumentException {
        if (!isViewPathValid(view)) {
            throw new IllegalArgumentException("Invalid view given!");
        }

        return VIEWS_BASE_DIR + view + VIEWS_FILE_EXT;
    }
}
