<%@tag description="Page Layout Tag" pageEncoding="UTF-8" %>
<%@attribute name="title" required="true" type="java.lang.String" %>
<%@attribute name="messages" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>${title}</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/main.css">
    </head>
    <body>
        <header>
            <jsp:invoke fragment="header"/>
        </header>
            <jsp:invoke fragment="messages"/>
            <jsp:doBody/>
        <footer>
            <jsp:invoke fragment="footer"/>
        </footer>
    </body>
</html>