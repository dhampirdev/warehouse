<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="currentDate" class="java.util.Date"/>
<div class="footer__wrapper container--base">
    <p>Copyright &copy; <f:formatDate value="${currentDate}" pattern="yyyy"/> Warehouse</p>
</div>
