<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container--base">
    <div class="navigation__menu">
        <span class="navigation__item">
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
        </span>
        <span class="navigation__item">
            <a href="<c:url value="/product/list"/>">Product List</a>
        </span>
    </div>
    <div class="logout-form__wrapper">
        <form method="POST" action="<c:url value="/logout"/>">
            <button class="action-button--primary" type="submit">Log out</button>
        </form>
    </div>
</div>