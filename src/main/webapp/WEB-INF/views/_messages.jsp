<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="messages-container">
    <c:if test="${not empty successMessage}">
        <p class="message success"><c:out value="${successMessage}"/></p>
    </c:if>
    <c:if test="${not empty errorMessage}">
        <p class="message error"><c:out value="${errorMessage}"/></p>
    </c:if>
</div>