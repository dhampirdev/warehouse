<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<layout:page title="Dashboard">
    <jsp:attribute name="header">
        <%@include file="_header.jsp"%>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <%@include file="_footer.jsp"%>
    </jsp:attribute>
    <jsp:body>
        <div id="dashboard-body" class="container--base">
            <h1>Dashboard</h1>
            <p>Welcome to <strong>Warehouse</strong> management. Use the navigation bar to access the product list.</p>
        </div>
    </jsp:body>
</layout:page>