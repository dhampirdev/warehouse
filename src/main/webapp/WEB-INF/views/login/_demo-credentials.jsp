<jsp:useBean id="demoCreds" class="dh.warehouse.demo.Credentials"/>
<div class="demo-credentials__container">
    <p>Use the following <strong>demo</strong> credentials to log in:</p>
    <ul>
        <li>Username: ${demoCreds.username}</li>
        <li>Password: ${demoCreds.password}</li>
    </ul>
</div>