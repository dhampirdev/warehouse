<h1>Warehouse Login</h1>
<div class="login-form__wrapper">
    <form id=login-form" method="POST" action="j_security_check">
        <div class="login-form__row">
            <label for="username">Username:</label>
            <input type="text" name="j_username" id="username">
        </div>
        <div class="login-form__row">
            <label for="password">Password:</label>
            <input type="password" name="j_password" id="password">
        </div>
        <div class="login-form__row">
            <button class="action-button--primary" type="submit">Login</button>
        </div>
    </form>
</div>