<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<layout:page title="Login Error">
    <jsp:body>
        <div id="login-error-body" class="container--centered container--narrow">
            <h1>Login Error</h1>
            <p>Please enter credentials for a user that is authorized to access this application.
                <br/>Click <a href="<c:url value="/" />">here</a> to try again.
            </p>
        </div>
    </jsp:body>
</layout:page>