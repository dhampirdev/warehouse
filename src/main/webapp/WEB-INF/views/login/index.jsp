<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<layout:page title="Login">
    <jsp:body>
        <div class="container--centered container--narrow">
            <%@include file="_form.jsp" %>
            <%@include file="_demo-credentials.jsp" %>
        </div>
    </jsp:body>
</layout:page>