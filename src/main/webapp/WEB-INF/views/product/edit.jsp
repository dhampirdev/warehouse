<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<layout:page title="${pageTitle}">
    <jsp:attribute name="messages">
        <jsp:include page="/WEB-INF/views/_messages.jsp" flush="true"/>
    </jsp:attribute>
    <jsp:attribute name="header">
        <%@include file="/WEB-INF/views/_header.jsp"%>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <%@include file="/WEB-INF/views/_footer.jsp"%>
    </jsp:attribute>
    <jsp:body>
        <div id="product-edit-body" class="container--base">
            <h1>${pageTitle}</h1>
            <div class="product-edit__back-button-wrapper">
                <%@include file="link/back-to-list.jsp" %>
            </div>
            <div class="product-edit-form-wrapper">
                <%@include file="form/_edit.jsp" %>
            </div>
        </div>
    </jsp:body>
</layout:page>