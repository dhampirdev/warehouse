<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="warehouse" uri="http://warehouse.local/jsp/tld/warehouse" %>
<%-- @elvariable id="formData" type="dh.warehouse.product.servlet.form.FormData"--%>
<form method="POST" action="<c:url value="/product/save"/>">
    <c:if test="${not empty productId}">
        <input type="hidden" name="id" value="${productId}"/>
    </c:if>

    <div class="form-fieldset">
        <div class="form-row">
            <label for="product.sku">SKU</label>
            <input id="product.sku" type="text" name="sku" value="${formData.sku}"/>
            <span class="error"><warehouse:formError field="sku"/></span>
        </div>

        <div class="form-row">
            <label for="product.name">Name</label>
            <input id="product.name" type="text" name="name" value="${formData.name}"/>
            <span class="error"><warehouse:formError field="name"/></span>
        </div>

        <div class="form-row">
            <label for="product.price">Price (USD)</label>
            <input id="product.price" type="text" name="price" value="${formData.price}"/>
            <span class="error"><warehouse:formError field="price"/></span>
        </div>

        <div class="form-row">
            <label for="product.qty">Quantity</label>
            <input id="product.qty" type="text" name="qty" value="${formData.qty}"/>
            <span class="error"><warehouse:formError field="qty"/></span>
        </div>

        <div class="save-button-wrapper">
            <button class="action-button--secondary" type="submit">Save</button>
        </div>
    </div>
</form>
