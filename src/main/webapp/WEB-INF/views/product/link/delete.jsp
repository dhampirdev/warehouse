<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form method="POST" action="<c:url value="/product/delete"/>">
    <input type="hidden" name="id" value="${param.productId}"/>
    <button class="action-button--caution" type="submit">Delete</button>
</form>