<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form method="GET" action="<c:url value="/product/edit"/>">
    <input type="hidden" name="id" value="${param.productId}"/>
    <button class="action-button--secondary" type="submit">Edit</button>
</form>