<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<layout:page title="Product List">
    <jsp:attribute name="messages">
        <jsp:include page="/WEB-INF/views/_messages.jsp" flush="true"/>
    </jsp:attribute>
    <jsp:attribute name="header">
        <%@include file="/WEB-INF/views/_header.jsp" %>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <%@include file="/WEB-INF/views/_footer.jsp" %>
    </jsp:attribute>
    <jsp:body>
        <div id="product-list-body" class="container--base">
            <h1>Products</h1>
            <div class="product-list__header">
                <div class="pagination-wrapper">
                    <%@include file="pagination/control.jsp" %>
                </div>
                <div class="product-list__add-button-wrapper">
                    <%@include file="link/add.jsp" %>
                </div>
            </div>
            <div class="product-list-wrapper">
                <table class="table--base product-list">
                    <thead>
                    <tr>
                        <th class="header__id">ID</th>
                        <th class="header__sku">SKU</th>
                        <th class="header__name">Name</th>
                        <th class="header__price">Price</th>
                        <th class="header__qty">Quantity</th>
                        <th class="header__actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <%--@elvariable id="products" type="java.util.Map<Integer, Product>"--%>
                        <%--@elvariable id="entry" type="java.util.Map.Entry"--%>
                    <c:forEach items="${products}" var="entry">
                        <c:set var="productId" value="${entry.key}"/>
                        <%--@elvariable id="product" type="dh.warehouse.product.Product"--%>
                        <c:set var="product" value="${entry.value}"/>
                        <tr>
                            <td><c:out value="${productId}"/></td>
                            <td><c:out value="${product.sku}"/></td>
                            <td><c:out value="${product.name}"/></td>
                            <td class="column--centered"><f:formatNumber
                                    type="CURRENCY"
                                    currencyCode="USD"
                                    minFractionDigits="2"
                                    maxFractionDigits="2"
                                    value="${product.price}"/>
                            </td>
                            <td class="column--centered"><c:out value="${product.qty}"/></td>
                            <td>
                                <div class="product-actions-container">
                                    <jsp:include page="link/edit.jsp">
                                        <jsp:param name="productId" value="${productId}"/>
                                    </jsp:include>
                                    <jsp:include page="link/delete.jsp">
                                        <jsp:param name="productId" value="${productId}"/>
                                    </jsp:include>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </jsp:body>
</layout:page>