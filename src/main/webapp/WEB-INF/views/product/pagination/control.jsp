<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- @elvariable id="pager" type="dh.warehouse.pager.Pager" --%>
<form method="GET" action="<c:url value="/product/list"/>">
    <button class="action-button--secondary" type="submit" name="p" value="1">Show</button>
    <select name="c">
        <c:forEach var="size" begin="2" step="2" end="16">
            <option value="${size}" <c:if test="${size eq pager.getPageSize()}">selected</c:if>>${size}</option>
        </c:forEach>
    </select>
    <span>per page</span>
    <c:if test="${pager.hasPreviousPage()}">
        <button class="action-button--secondary" type="submit" name="p" value="${pager.getPreviousPageNumber()}">
            Previous
        </button>
    </c:if>
    <c:if test="${pager.hasNextPage()}">
        <button class="action-button--secondary" type="submit" name="p" value="${pager.getNextPageNumber()}">Next
        </button>
    </c:if>
</form>