package dh.warehouse.pager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PagerTest {

    private Pager pager;

    @Before
    public void setUp() {
        pager = new Pager();
    }

    @Test
    public void withNoItemCountHasNoPreviousPage() {
        Assert.assertFalse(pager.hasPreviousPage());
    }

    @Test
    public void withNoItemCountHasNoNextPage() {
        Assert.assertFalse(pager.hasNextPage());
    }

    @Test
    public void onPageOutOfBoundsHasNoPreviousPage() {
        pager.setPageNumber(3);
        pager.setPageSize(10);
        pager.setTotalItemCount(10);

        Assert.assertFalse(pager.hasPreviousPage());
    }

    @Test
    public void onPageOutOfBoundsHasNoNextPage() {
        pager.setPageNumber(2);
        pager.setPageSize(10);
        pager.setTotalItemCount(10);

        Assert.assertFalse(pager.hasNextPage());
    }

    @Test
    public void onMiddlePageHasPreviousPage() {
        pager.setPageNumber(2);
        pager.setPageSize(10);
        pager.setTotalItemCount(21);

        Assert.assertTrue(pager.hasPreviousPage());
    }

    @Test
    public void onMiddlePageHasNextPage() {
        pager.setPageNumber(2);
        pager.setPageSize(10);
        pager.setTotalItemCount(21);

        Assert.assertTrue(pager.hasNextPage());
    }

    @Test
    public void onSinglePageHasNoPreviousPage() {
        pager.setPageNumber(1);
        pager.setPageSize(10);
        pager.setTotalItemCount(10);

        Assert.assertFalse(pager.hasPreviousPage());
    }

    @Test
    public void onSinglePageHasNoNextPage() {
        pager.setPageNumber(1);
        pager.setPageSize(10);
        pager.setTotalItemCount(10);

        Assert.assertFalse(pager.hasNextPage());
    }

    @Test
    public void onLastPageHasPreviousPage() {
        pager.setPageNumber(2);
        pager.setPageSize(10);
        pager.setTotalItemCount(11);

        Assert.assertTrue(pager.hasPreviousPage());
    }

    @Test
    public void onLastPageHasNoNextPage() {
        pager.setPageNumber(2);
        pager.setPageSize(10);
        pager.setTotalItemCount(11);

        Assert.assertFalse(pager.hasNextPage());
    }
}
